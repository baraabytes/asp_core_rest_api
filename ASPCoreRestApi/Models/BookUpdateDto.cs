﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCoreRestApi.Models
{
    public class BookUpdateDto
    {
        public string Title { get; set; }
        public string Descriptions { get; set; }
    }
}
