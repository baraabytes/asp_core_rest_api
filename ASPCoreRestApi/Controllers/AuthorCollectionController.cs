﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPCoreRestApi.Helper;
using ASPCoreRestApi.Models;
using AutoMapper;
using DataLayer;
using DataLayer.Entities;
using Microsoft.AspNetCore.Mvc;

namespace ASPCoreRestApi.Controllers
{
    [Route("api/authorcollection")]
    public class AuthorCollectionController : Controller
    {
        private ILibraryRepository repository;
        public AuthorCollectionController(ILibraryRepository  libraryRepository)
        {
            this.repository = libraryRepository;
        }
        [HttpGet("({ids})",Name = "GetAuthorsForIds")]
       public IActionResult GetAuthorsForIds(
            [ModelBinder(BinderType=typeof(ArrayModelBinder))]IEnumerable<int> ids)
        {
            if (!ids.Any() || ids == null)
            {
                return BadRequest();
            }
            var authorsEntities = this.repository.GetAuthors(ids);
            if(authorsEntities == null || !authorsEntities.Any())
            {
                return NotFound();
            }
            var authorsDtos = Mapper.Map<IEnumerable<AuthorDto>>(authorsEntities);
            return Ok(authorsDtos);

        }

        [HttpPost]
        public IActionResult CreateAuthors([FromBody] IEnumerable<AuthorCreationDto> authorsCollection)
        {
            if (!authorsCollection.Any() || authorsCollection == null) return BadRequest();
            var authorEntities = Mapper.Map<IEnumerable<Authors>>(authorsCollection);
            

            authorEntities.ToList().ForEach(author => repository.AddAuthor(author));
            if (!repository.Save())
            {
                return StatusCode(500, "Error! Problem While Processing your request...");
            }
            int[] authorsIds = authorEntities.Select(x => x.Id).ToArray();
            string idsStr = string.Join(',', authorsIds);
            IEnumerable<AuthorDto> authorsDto = Mapper.Map<IEnumerable<AuthorDto>>(authorEntities);
            return CreatedAtRoute("GetAuthorsForIds", new { ids = idsStr }, authorsDto);
        }
    }
}