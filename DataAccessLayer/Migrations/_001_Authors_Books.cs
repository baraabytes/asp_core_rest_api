﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataAccessLayer.Migrations
{
    [Migration(1)]
    public class _001_Authors_Books : Migration
    {
        public override void Up()
        {
            //DB AspCoreApi
            Create.Table("Authors").WithColumn("id").AsInt32().PrimaryKey().Identity()
                .WithColumn("first_name").AsCustom("nvarchar(50)").NotNullable()
                .WithColumn("last_name").AsCustom("nvarchar(50)").NotNullable()
                .WithColumn("date_of_birth").AsDateTime()
                .WithColumn("genre").AsString(50);
            Create.Table("Books").WithColumn("id").AsInt32().PrimaryKey().Identity()
                .WithColumn("title").AsString(100).NotNullable()
                .WithColumn("description").AsString(500)
                .WithColumn("author_id").AsInt32().ForeignKey("author_id", "Authors", "id").OnDelete(Rule.Cascade);
           
        }
        public override void Down()
        {
            Delete.Table("Authors");
            Delete.Table("Books");
        }
    }
}
