﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ASPCoreRestApi.Helper
{
    public class ArrayModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (!bindingContext.ModelMetadata.IsEnumerableType)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.CompletedTask;
            }

            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).ToString();
            if (string.IsNullOrWhiteSpace(value))
            {
                bindingContext.Result = ModelBindingResult.Success(null);
                return Task.CompletedTask;
            }
            var type = bindingContext.ModelType.GetTypeInfo().GenericTypeArguments[0];
            var converter = TypeDescriptor.GetConverter(type);

            var arryValue = value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => converter.ConvertFromString(x.Trim())).ToArray();

            var typeValues = Array.CreateInstance(type, arryValue.Length);
            arryValue.CopyTo(typeValues, 0);

            bindingContext.Model = typeValues;
            bindingContext.Result = ModelBindingResult.Success(bindingContext.Model);
            return Task.CompletedTask;
        }
    }
}


/*********
 * Attributes could be used with Binding object to select the binding criteria
 *
 *
    [BindRequired]: If binding cannot happen, this attribute adds a ModelState error.
    [BindNever]: Tells the model binder to ignore this parameter.
    [FromHeader]: Forces binding from the HTTP request header.
    [FromQuery]: Forces binding from the URL's query string.
    [FromServices]: Binds the parameter from services provided by dependency injection.
    [FromRoute]: Forces binding from values provided by Routing.
    [FromForm]: Forces binding from values in the FORM.
    [FromBody]: Forces binding from values in the body of the HTTP request.

 * 
 * 
 * ********/