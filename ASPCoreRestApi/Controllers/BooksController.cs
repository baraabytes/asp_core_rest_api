﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPCoreRestApi.Models;
using AutoMapper;
using DataLayer;
using DataLayer.Entities;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace ASPCoreRestApi.Controllers
{
    [Route("api/authors/{authorId}/books")]
    public class BooksController : Controller
    {
        private ILibraryRepository libraryRepository;
        public BooksController(ILibraryRepository repository)
        {
            libraryRepository = repository;
        }
        [HttpGet()]
        public IActionResult GetBooks(int authorId)
        {
            if (!libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }

            IEnumerable<BookDto> result = Mapper.Map<IEnumerable<BookDto>>(libraryRepository.GetBooksForAuthor(authorId));

            return Ok(result);
        }
        [HttpGet("{id}",Name = "GetBook")]
        public IActionResult GetBook(int authorId,int id)
        {
            if (!libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }
            var book = libraryRepository.GetBookForAuthor(authorId, id);
            if (book == null) return NotFound();
            return Ok(book);
        }
        [HttpPost()]
        public IActionResult CreateBookForAuthor(int authorId, [FromBody] BookCreationDto bookDto)
        {

            if (bookDto == null)
            {
                return BadRequest();
            }

            if(bookDto.Title == bookDto.Descriptions)
            {
                ModelState.AddModelError(nameof(BookCreationDto), "Provided Description should be different from the title");
            }

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            if (!libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }
      
            var bookEntity = Mapper.Map<Books>(bookDto);
            libraryRepository.AddBookForAuthor(authorId, bookEntity);
            if (!libraryRepository.Save()) return StatusCode(500, "Error! Problem while handling your request...");
            var result = Mapper.Map<BookDto>(bookEntity);

            return CreatedAtRoute("GetBook", new { authorId = result.AuthorId, id = result.Id }, result);

        }

        [HttpDelete("{bookId}")]
        public IActionResult DeleteBookForAuthor(int authorId,int bookId)
        {
            if (!libraryRepository.AuthorExists(authorId))
            {
                return NotFound();
            }
            var book = libraryRepository.GetBookForAuthor(authorId, bookId);
            if (book == null) return NotFound();
            libraryRepository.DeleteBook(book);
            if (!libraryRepository.Save()) return StatusCode(500, "Error! failed processing your request...");

            return NoContent();
        }

        [HttpPut("{bookId}")]
        public IActionResult UpdateBookForAuthor(int authorId,int bookId,[FromBody] BookUpdateDto updateDto)
        {
            var bookEntity = libraryRepository.GetBookForAuthor(authorId, bookId);
            if (!libraryRepository.AuthorExists(authorId) || bookEntity == null) return NotFound();
            if (updateDto == null) return BadRequest();
            var updatedBook = Mapper.Map(updateDto, bookEntity);
            libraryRepository.UpdateBookForAuthor(updatedBook);
            if (!libraryRepository.Save())
            {
                return StatusCode(500, "Error! failed processing your request...");
            }
            return Ok(updatedBook);
        }

        [HttpPatch("{bookId}")]
        public IActionResult PartiallyUpdateBookForAuthor(int authorId,int bookId, [FromBody] JsonPatchDocument<BookUpdateDto> patchDocs )
        {
            var bookEntity = libraryRepository.GetBookForAuthor(authorId, bookId);
            if (!libraryRepository.AuthorExists(authorId) || bookEntity == null) return NotFound();
            if (patchDocs == null) return BadRequest();

            var bookToPatch = Mapper.Map<BookUpdateDto>(bookEntity);

            patchDocs.ApplyTo(bookToPatch,ModelState);

            TryValidateModel(ModelState);
            if (!ModelState.IsValid)
                return new UnprocessableEntityObjectResult(ModelState);

            Mapper.Map(bookToPatch, bookEntity);
            libraryRepository.UpdateBookForAuthor(bookEntity);
            if (!libraryRepository.Save())
            {
                return StatusCode(500, "Error! failed processing your request...");

            }
            return NoContent();
        }
    }
}