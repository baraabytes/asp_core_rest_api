﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPCoreRestApi.Models;
using AutoMapper;
using DataLayer;
using DataLayer.Entities;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ASPCoreRestApiContext.Controllers
{
    [Route("api/authors")]
    public class AuthorsController : Controller
    {
        private ILibraryRepository libraryRepository;
        private readonly ILogger _logger;
        public AuthorsController(ILibraryRepository repository, ILogger<Startup> logger)
        {
            this.libraryRepository = repository;
            _logger = logger;
        }
        [HttpGet()]
        public IActionResult GetAuthors()
        {
            var result = Mapper.Map<IEnumerable<AuthorDto>>(libraryRepository.GetAuthors());

            return new JsonResult(result); 
        }

        [HttpGet("{id}",Name ="GetAuthor")]
        public IActionResult GetAuthor(int id)
        {
            var result = Mapper.Map<AuthorDto>(libraryRepository.GetAuthor(id));
            if (result == null)
                return NotFound();
            return Ok(result);
        }

        [HttpPost]
        public IActionResult CreatAuthor([FromBody] AuthorCreationDto authorDto)
        {
            if (authorDto == null) return BadRequest();
            try
            {
                throw new Exception("BIG ERROR! TESTING Nlog....");
            }
            catch (Exception e)
            {
                _logger.LogError(e,null);
            }
            var author = Mapper.Map<Authors>(authorDto);
            libraryRepository.AddAuthor(author);
            bool isSuccessful = libraryRepository.Save();
            if (! isSuccessful) return StatusCode(500, "Error! While handling your request...");

            var result = Mapper.Map<AuthorDto>(author);
            return CreatedAtRoute("GetAuthor", new { id = result.Id }, result);

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteAuthor(int id)
        {
            var author = libraryRepository.GetAuthor(id);
            if (author == null) return NotFound();

            libraryRepository.DeleteAuthor(author);
            if (!libraryRepository.Save()) return StatusCode(500, "Error! failed to process your request...");
            return NoContent();
        }

        [HttpPatch("{id}")]
        public IActionResult PartiallyUpdateAuthor(int id,[FromBody]JsonPatchDocument<AuthorUpdateDto> patchDocs)
        {
            var authorEntity = libraryRepository.GetAuthor(id);
            if (authorEntity == null) return NotFound();
            if (patchDocs == null) return BadRequest();
            var authorToPatch = Mapper.Map<AuthorUpdateDto>(authorEntity);
            patchDocs.ApplyTo(authorToPatch);

            Mapper.Map(authorToPatch, authorEntity);
            if (!libraryRepository.Save())
            {
                return StatusCode(500, "Error! failed to process your request...");
            }
            return NoContent();
        }
    }
}
