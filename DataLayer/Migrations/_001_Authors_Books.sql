﻿CREATE TABLE Authors (
	id int not null PRIMARY KEY Identity,
    first_name nvarchar(60) NOT NULL,
	last_name nvarchar(60) Not null,
    date_of_birth dateTime,
	genre nvarchar(60)
)
Go
CREATE TABLE Books (
	id int not null PRIMARY KEY Identity,
    title nvarchar(100) NOT NULL,
	descriptions nvarchar(500),
    author_id int FOREIGN KEY REFERENCES Authors(id)
)
/******
##For creating Entities and DbContext first time::>
Scaffold-DbContext "Server=BARAAABUZAIFBD4\SQLEXPRESS;Initial Catalog=AspCoreApi;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Entities
##For overide existing Entities and DbContext ::>
Scaffold-DbContext "Server=BARAAABUZAIFBD4\SQLEXPRESS;Initial Catalog=AspCoreApi;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Entities -force
******/

/*****
## For using Guid As Primary Key ::>
	USE MathDB
	GO
 
	CREATE TABLE MathStudents1
	(
		Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
		StudentName VARCHAR (50)
 
	)
	GO
 
	INSERT INTO MathStudents1 VALUES (default,'Sally')
	INSERT INTO MathStudents1 VALUES (default,'Edward')
*****/