﻿
using DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCoreRestApi.Helper
{
    public static class AuthorExt
    {
        public static int DateTimeToAge(this Authors author)
        {
            return int.Parse((DateTime.Now.Year - author.DateOfBirth.Year).ToString());
        }
    }
}
