﻿using System;
using System.Collections.Generic;

namespace DataLayer.Entities
{
    public partial class Books
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public int? AuthorId { get; set; }

        public virtual Authors Author { get; set; }
    }
}
