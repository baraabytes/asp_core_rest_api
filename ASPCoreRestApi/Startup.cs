﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ASPCoreRestApi.Helper;

using ASPCoreRestApi.Models;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Diagnostics;

namespace ASPCoreRestApiContext
{
    public class Startup
    {

        public IConfiguration Configuration { get; }
        private readonly ILogger _logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

      

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // to access connection string ::> Configuration["ConnectionStrings:AppDb"]
            services.AddMvc(setupAction=>
            {
                setupAction.ReturnHttpNotAcceptable = true;
                setupAction.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
            });//.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            services.AddDbContext<AspCoreApiContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("AppDb")));
            services.AddScoped<ILibraryRepository, LibraryRepository>();
         
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Use(async (context,next) =>
               {
                   var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                   if(exceptionHandlerFeature != null)
                   {
                       _logger.LogError(500,
                           exceptionHandlerFeature.Error,
                           exceptionHandlerFeature.Error.Message);
                   }

                   await next();

               });

            });

            AutoMapper.Mapper.Initialize(MapConfig =>
            {
                MapConfig.CreateMap<Authors, AuthorDto>()
                .ForMember(destObj => destObj.Name, opt => opt.MapFrom(srcObj => $"{srcObj.FirstName} {srcObj.LastName}"))
                .ForMember(obj => obj.Age, opt => opt.MapFrom(src => src.DateTimeToAge() ));

                MapConfig.CreateMap<Books, BookDto>();
                MapConfig.CreateMap<AuthorCreationDto, Authors>();
                MapConfig.CreateMap<BookCreationDto, Books>();
                MapConfig.CreateMap<BookUpdateDto, Books>();
                MapConfig.CreateMap<AuthorUpdateDto, Authors>();
                // <Source,Destination>

            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
