﻿
using DataLayer.Entities;
using System;
using System.Collections.Generic;

namespace DataLayer
{
    public interface ILibraryRepository
    {
        IEnumerable<Authors> GetAuthors();
        Authors GetAuthor(int authorId);
        IEnumerable<Authors> GetAuthors(IEnumerable<int> authorIds);
        void AddAuthor(Authors author);
        void DeleteAuthor(Authors author);
        void UpdateAuthor(Authors author);
        bool AuthorExists(int authorId);
        IEnumerable<Books> GetBooksForAuthor(int authorId);
        Books GetBookForAuthor(int authorId, int bookId);
        void AddBookForAuthor(int authorId, Books book);
        void UpdateBookForAuthor(Books book);
        void DeleteBook(Books book);
        bool Save();
    }
}
