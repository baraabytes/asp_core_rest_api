﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Entities;

namespace DataLayer
{
    public class LibraryRepository : ILibraryRepository
    {
        private AspCoreApiContext DbContext;
        public LibraryRepository(AspCoreApiContext dbContext)
        {
            DbContext = dbContext;
        }
        public void AddAuthor(Authors author)
        {
            DbContext.Authors.Add(author);
        }

        public void AddBookForAuthor(int authorId, Books book)
        {
            Authors author = GetAuthor(authorId);
            if (author == null) return;
            author.Books.Add(book);
            
        }

        public bool AuthorExists(int authorId)
        {
            return DbContext.Authors.Any<Authors>(x => x.Id == authorId);
        }

        public void DeleteAuthor(Authors author)
        {
            DbContext.Authors.Remove(author);
        }

        public void DeleteBook(Books book)
        {
            DbContext.Books.Remove(book);
        }

        public Authors GetAuthor(int authorId)
        {
           return DbContext.Authors.FirstOrDefault<Authors>(x => x.Id == authorId);        
        }

        public IEnumerable<Authors> GetAuthors()
        {
            return DbContext.Authors.OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
        }

        public IEnumerable<Authors> GetAuthors(IEnumerable<int> authorIds)
        {
            return authorIds.Select<int, Authors>(x => GetAuthor(x)).Where(x=>x!=null).OrderBy(x=>x.FirstName)
                .ThenBy(x=>x.LastName).ToList<Authors>();
        }

        public Books GetBookForAuthor(int authorId, int bookId)
        {
            return GetBooksForAuthor(authorId).Where(x => x.Id == bookId).FirstOrDefault();
        }

        public IEnumerable<Books> GetBooksForAuthor(int authorId)
        {
            return DbContext.Books.Where(x => x.AuthorId == authorId);
        }

        public bool Save()
        {
            return DbContext.SaveChanges() > 0;
        }

        public void UpdateAuthor(Authors author)
        {

        }

        public void UpdateBookForAuthor(Books book)
        {
            
        }
    }
}
