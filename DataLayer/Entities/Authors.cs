﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities
{
    public partial class Authors
    {
        public Authors()
        {
            Books = new HashSet<Books>();
        }
       

        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Genre { get; set; }

        public virtual ICollection<Books> Books { get; set; }
    }
}
